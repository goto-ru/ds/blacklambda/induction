module Induction where

data Nat = O | S Nat

instance Enum Nat where
    succ x = S x

    pred O = undefined
    pred (S x) = x

    toEnum 0 = O
    toEnum x = S $ toEnum $ x - 1

    fromEnum O = 0
    fromEnum (S x) = 1 + fromEnum x

instance Show Nat where
    show = show . fromEnum
