{ mkDerivation, base, stdenv }:
mkDerivation {
  pname = "induction";
  version = "0.1.0.0";
  src = ./.;
  libraryHaskellDepends = [ base ];
  license = stdenv.lib.licenses.agpl3;
}
